//
//  HowToViewController.m
//  CoffeeTimer
//
//  Created by John Johnson on 9/2/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import "HowToViewController.h"

@interface HowToViewController ()
- (IBAction)changeScreen:(id)sender;

@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UILabel *screenNumber;

@end

@implementation HowToViewController{
    NSArray *howToSteps;
    int howToInstructions;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *c = [NSString stringWithFormat:@"%@", self.currentBrew.name];
    if ([c  isEqualToString:@"Aeropress"] ){
    }
    else if ([c isEqualToString:@"Clever Drip"] ){
        howToSteps = @[@"Set water to approximately 200 degrees", @"Measure 22 grams of coffee and grind at the setting shown below", @"Fold filter, place in clever, wet with hot water, pour out, and add your ground coffee", @"Blooming the coffee with 75-100grams of coffee and strir 5 times back and forth", @"Fill to 350-365 mL's then stir 5 times over the course of 40 seconds", @"Cap the clever drip and let it sit for one minute", @"Let the clever drop until the coffee is all gone. Should be 90 seconds make the grind more coarse if it is longer and finer if it was shorter"];
            // The number of items reflected in the page indicator.
        
    }
    else if ([c isEqualToString:@"French Press"] ){
    }
    else if ([c isEqualToString:@"V60"] ){
    }
    else if ([c isEqualToString:@"Chemex"] ){
    }
    
}

- (IBAction)changeScreen:(id)sender
{
    self.screenNumber.text = [NSString stringWithFormat:@"%i", ([self.pageControl currentPage]+1)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
