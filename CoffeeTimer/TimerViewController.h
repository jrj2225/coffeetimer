//
//  TimerViewController.h
//  CoffeeTimer
//
//  Created by John Johnson on 7/21/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SelectionTableViewController.h"
#import "DecisionViewController.h"


@interface TimerViewController : UIViewController <UITableViewDataSource> {
    IBOutlet UILabel* time;
    IBOutlet UILabel* downTime;
    IBOutlet UILabel* nextAction;
    IBOutlet UILabel* current;
    IBOutlet UILabel *grams;
    IBOutlet UILabel *mL;
   // NSTimer* myTicker;
    int secondsPassed;
    int secondsLeft;
    NSArray *timeCount;
    int secondsCount;
    int count;
    AVAudioPlayer *buttonBeep;
    int stopCount;
}
@property (nonatomic) PhotoIcon *currentBrew;
@property (nonatomic, retain) NSTimer *myTicker;
- (void) showActivity;
//- (void) resetTimer;


@end
