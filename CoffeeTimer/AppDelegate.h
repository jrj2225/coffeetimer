//
//  AppDelegate.h
//  CoffeeTimer
//
//  Created by John Johnson on 7/15/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
