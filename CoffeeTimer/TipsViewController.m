//
//  TipsViewController.m
//  CoffeeTimer
//
//  Created by John Johnson on 8/25/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import "TipsViewController.h"

@interface TipsViewController ()

@end

@implementation TipsViewController

NSInteger instructions;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *typeOfBrew = [NSString stringWithFormat:@"%@", self.currentBrew.name];
    // Do any additional setup after loading the view.
    self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
    if ([typeOfBrew isEqualToString: @"Aeropress"]) {
        tipsSteps = @[@"Prepare", @"Bloom and Stir",@"Full Pour", @"Flip and Press", @"Enjoy!"];
        tipsMuchTime = [NSArray arrayWithObjects:
                       [NSNumber numberWithInt:3],
                       [NSNumber numberWithInt:30],
                       [NSNumber numberWithInt:40],
                       [NSNumber numberWithInt:60],
                       [NSNumber numberWithInt:0],
                       nil];
    }
    else if ([typeOfBrew isEqualToString:@"Clever Drip"] ){
        tipsSteps = @[@"Prepare", @"Bloom and Stir", @"Fill and Stir", @"Cap", @"Drop", @"Enjoy!"];//TBD
        self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
        tipsMuchTime = [NSArray arrayWithObjects:
                     [NSNumber numberWithInt:3],//TBD
                     [NSNumber numberWithInt:20],//TBD
                     [NSNumber numberWithInt:40],//TBD
                     [NSNumber numberWithInt:60],//TBD
                     [NSNumber numberWithInt:90],//TBD
                     [NSNumber numberWithInt:0],//TBD
                     nil];
    }
    else if ([typeOfBrew isEqualToString:@"French Press"] ){
        tipsSteps = @[@"Prepare", @"Half Pour", @"Wait", @"Stir and Fill", @"Wait", @"Press", @"Enjoy!"];//TBD
        self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
        tipsMuchTime = [NSArray arrayWithObjects:
                     [NSNumber numberWithInt:3],//TBD
                     [NSNumber numberWithInt:30],//TBD
                     [NSNumber numberWithInt:30],//TBD
                     [NSNumber numberWithInt:40],//TBD
                     [NSNumber numberWithInt:140],//TBD
                     [NSNumber numberWithInt:20],//TBD
                     [NSNumber numberWithInt:0],//TBD
                     nil];
    }
    else if ([typeOfBrew isEqualToString:@"V60"] ){
        tipsSteps = @[@"Prepare", @"Bloom", @"Full Pour", @"Enjoy!"];//TBD
        self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
        tipsMuchTime = [NSArray arrayWithObjects:
                     [NSNumber numberWithInt:3],//TBD
                     [NSNumber numberWithInt:35],//TBD
                     [NSNumber numberWithInt:85],//TBD
                     [NSNumber numberWithInt:0],//TBD
                     nil];
    }
    else if ([typeOfBrew isEqualToString:@"Chemex"] ){
        tipsSteps = @[@"Prepare", @"Bloom", @"3 Pulse Pour", @"Complete Drip", @"Enjoy!"];//TBD
        self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
        tipsMuchTime = [NSArray arrayWithObjects:
                     [NSNumber numberWithInt:3],//TBD
                     [NSNumber numberWithInt:55],//TBD
                     [NSNumber numberWithInt:125],//TBD
                     [NSNumber numberWithInt:60],//TBD
                     [NSNumber numberWithInt:0],//TBD
                     nil];
    }

    instructions = tipsSteps.count;
}

// Sets up the Table View
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return instructions;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"TipsCell"];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", tipsSteps[indexPath.row]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ Seconds", tipsMuchTime[indexPath.row]];
    return cell;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
