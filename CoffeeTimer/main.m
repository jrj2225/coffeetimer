//
//  main.m
//  CoffeeTimer
//
//  Created by John Johnson on 7/15/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
