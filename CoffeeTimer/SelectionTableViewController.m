//
//  SelectionTableViewController.m
//  CoffeeTimer
//
//  Created by John Johnson on 7/21/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import "SelectionTableViewController.h"

@interface SelectionTableViewController (){
    NSMutableArray  *type;
}

@end

@implementation SelectionTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    type = [[NSMutableArray alloc]init];

    PhotoIcon *pic = [[PhotoIcon alloc]init];
    pic.name = @"Clever Drip";
    pic.fileName = @"Clever";
    [type addObject:pic];
    
    pic = [[PhotoIcon alloc]init];
    pic.name = @"French Press";
    pic.fileName = @"Press";
    [type addObject:pic];
    
    pic = [[PhotoIcon alloc]init];
    pic.name = @"Aeropress";
    pic.fileName = @"Aeropress";
    [type addObject:pic];
    
    pic = [[PhotoIcon alloc]init];
    pic.name = @"V60";
    pic.fileName = @"V60";
    [type addObject:pic];
    
    pic = [[PhotoIcon alloc]init];
    pic.name = @"Chemex";
    pic.fileName = @"Chemex";
    [type addObject:pic];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return type.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    PhotoIcon *current = [type objectAtIndex:indexPath.row];
    cell.textLabel.text = [current name];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",current.fileName]];
    cell.imageView.image = imgView.image;
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    DecisionViewController *dvc = [segue destinationViewController];
    // Get the new view controller using [segue destinationViewController].
    
  
    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    PhotoIcon *c = type[path.row];
    [dvc setCurrentBrew:c];
    
    // Pass the selected object to the new view controller.
}


@end
