//
//  TipsViewController.h
//  CoffeeTimer
//
//  Created by John Johnson on 8/25/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SelectionTableViewController.h"
#import "DecisionViewController.h"
#import "TimerViewController.h"

@interface TipsViewController : UIViewController{
    IBOutlet UILabel* timeForTips;
    IBOutlet UILabel* actionsForTips;
    NSArray *tipsSteps;
    NSArray *tipsMuchTime;
}


@property (nonatomic) PhotoIcon *currentBrew;

@end
