//
//  DecisionViewController.m
//  CoffeeTimer
//
//  Created by John Johnson on 7/21/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import "DecisionViewController.h"

@interface DecisionViewController ()

@end

@implementation DecisionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
   // NSString *typeOfBrew = [NSString stringWithFormat:@"%@", self.currentBrew.name];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    TimerViewController *tvc = [segue destinationViewController];
    // Get the new view controller using [segue destinationViewController].
    PhotoIcon *c = self.currentBrew;
    [tvc setCurrentBrew:c];

}
@end
