//
//  PhotoIcon.h
//  CoffeeTimer
//
//  Created by John Johnson on 7/21/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoIcon : NSObject
@property  (nonatomic) NSString *name;
@property  (nonatomic) NSString *fileName;

@end
