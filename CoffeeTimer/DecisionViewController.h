//
//  DecisionViewController.h
//  CoffeeTimer
//
//  Created by John Johnson on 7/21/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectionTableViewController.h"
#import "TimerViewController.h"


@interface DecisionViewController : UIViewController
@property (nonatomic) PhotoIcon *currentBrew;
@end
