//
//  PreCoffeeViewController.h
//  CoffeeTimer
//
//  Created by John Johnson on 7/21/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoIcon.h"

@interface PreCoffeeViewController : UIViewController

@property (nonatomic) PhotoIcon *currentPhoto;

@end
