//
//  HowToViewController.h
//  CoffeeTimer
//
//  Created by John Johnson on 9/2/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SelectionTableViewController.h"
#import "DecisionViewController.h"

@interface HowToViewController : UIViewController{
}

@property (nonatomic) PhotoIcon *currentBrew;
@end
