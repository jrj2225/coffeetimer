//
//  SelectionTableViewController.h
//  CoffeeTimer
//
//  Created by John Johnson on 7/21/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoIcon.h"
#import "TimerViewController.h"
#import "DecisionViewController.h"

@interface SelectionTableViewController : UITableViewController
@property (nonatomic) PhotoIcon *currentBrew;
@end
