//
//  ViewController.m
//  CoffeeTimer
//
//  Created by John Johnson on 7/15/14.
//  Copyright (c) 2014 JohnJohnson. All rights reserved.
//

#import "TimerViewController.h"

@interface TimerViewController()
//@property (weak, nonatomic) IBOutlet UIImageView *currentImage;

@end


@implementation TimerViewController{
    int instructions;
    int gram;
    int milliliter;
    NSArray *steps;
    NSString *action;
}

// Sets up the Table View
/*-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return instructions;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", steps[indexPath.row]];
    
    return cell;
}*/


// How to make the BEEP
- (AVAudioPlayer *)setupAudioPlayerWithFile:(NSString *)file type:(NSString *)type
{
    NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:type];
    NSURL *beepUrl = [NSURL fileURLWithPath:path];
    NSError *error;
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepUrl error:&error];
    if (!audioPlayer) {
        NSLog(@"%@",[error description]);
    }
    return audioPlayer;
}

// Where to put the steps and time for different methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    //Do any additional setup after loading the view, typically from a nib.
    NSString *c = [NSString stringWithFormat:@"%@", self.currentBrew.name];
    stopCount = 0;
    
    if ([c  isEqualToString:@"Aeropress"] ){
        // these are determined off of what is selected
        steps = @[@"Prepare", @"Bloom and Stir",@"Full Pour", @"Flip and Press", @"Enjoy!"];// these are determined off of what is selected
        instructions = steps.count;
        milliliter = 250;
        gram = 17;
        grams.text = [NSString stringWithFormat:@"Grams: %i", gram];
        mL.text = [NSString stringWithFormat:@"mLs: %i",milliliter];
        self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
        timeCount = [NSArray arrayWithObjects: //this part needs to be determined off of what is selected
                     [NSNumber numberWithInt:3],
                     [NSNumber numberWithInt:30],
                     [NSNumber numberWithInt:40],
                     [NSNumber numberWithInt:60],
                     [NSNumber numberWithInt:0],
                     nil];
    }
    else if ([c isEqualToString:@"Clever Drip"] ){
        steps = @[@"Prepare", @"Bloom and Stir", @"Fill and Stir", @"Cap", @"Drop", @"Enjoy!"];//TBD
        instructions = steps.count;
        milliliter = 365;
        gram = 22;
        grams.text = [NSString stringWithFormat:@"Grams: %i", gram];
        mL.text = [NSString stringWithFormat:@"mLs: %i",milliliter];
        self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
        timeCount = [NSArray arrayWithObjects:
                     [NSNumber numberWithInt:3],//TBD
                     [NSNumber numberWithInt:20],//TBD
                     [NSNumber numberWithInt:40],//TBD
                     [NSNumber numberWithInt:60],//TBD
                     [NSNumber numberWithInt:90],//TBD
                     [NSNumber numberWithInt:0],//TBD
                     nil];
    }
        else if ([c isEqualToString:@"French Press"] ){
            steps = @[@"Prepare", @"Half Pour", @"Wait", @"Stir and Fill", @"Wait", @"Press", @"Enjoy!"];//TBD
            instructions = steps.count;
            milliliter = 340;
            gram = 20;
            grams.text = [NSString stringWithFormat:@"Grams: %i", gram];
            mL.text = [NSString stringWithFormat:@"mLs: %i",milliliter];
            self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
            timeCount = [NSArray arrayWithObjects:
                         [NSNumber numberWithInt:3],//TBD
                         [NSNumber numberWithInt:30],//TBD
                         [NSNumber numberWithInt:30],//TBD
                         [NSNumber numberWithInt:40],//TBD
                         [NSNumber numberWithInt:140],//TBD
                         [NSNumber numberWithInt:20],//TBD
                         [NSNumber numberWithInt:0],//TBD
                         nil];
        }
        else if ([c isEqualToString:@"V60"] ){
            steps = @[@"Prepare", @"Bloom", @"Full Pour", @"Enjoy!"];//TBD
            instructions = steps.count;
            milliliter = 250;
            gram = 17;
            grams.text = [NSString stringWithFormat:@"Grams: %i", gram];
            mL.text = [NSString stringWithFormat:@"mLs: %i",milliliter];
            self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
            timeCount = [NSArray arrayWithObjects:
                         [NSNumber numberWithInt:3],//TBD
                         [NSNumber numberWithInt:35],//TBD
                         [NSNumber numberWithInt:85],//TBD
                         [NSNumber numberWithInt:0],//TBD
                         nil];
        }
        else if ([c isEqualToString:@"Chemex"] ){
            steps = @[@"Prepare", @"Bloom", @"3 Pulse Pour", @"Complete Drip", @"Enjoy!"];//TBD
            instructions = steps.count;
            milliliter = 250;
            gram = 17;
            grams.text = [NSString stringWithFormat:@"Grams: %i", gram];
            mL.text = [NSString stringWithFormat:@"mLs: %i",milliliter];
            self.navigationItem.title = [NSString stringWithFormat:@"%@", self.currentBrew.name];
            timeCount = [NSArray arrayWithObjects:
                         [NSNumber numberWithInt:3],//TBD
                         [NSNumber numberWithInt:55],//TBD
                         [NSNumber numberWithInt:125],//TBD
                         [NSNumber numberWithInt:60],//TBD
                         [NSNumber numberWithInt:0],//TBD
                         nil];
        }
    
       // else if ([c isEqualToString:@"Custom"] ){
       
}

// The Start Button
- (IBAction)buttonTapped:(id)sender
{
    stopCount++;
    int remainder;
    remainder = stopCount % 2;
    if (stopCount == 1){
        buttonBeep = [self setupAudioPlayerWithFile:@"ButtonTap" type:@"wav"];
        [self.myTicker invalidate];
        [self setupTimer];
    }
    else if (remainder == 0) {
        [self.myTicker invalidate];
    }
    else
        [self fireTimer];
}

// The Reset Button
- (IBAction)stopButtonTapped:(id)sender
{
    buttonBeep = [self setupAudioPlayerWithFile:@"ButtonTap" type:@"wav"];
    [self.myTicker invalidate];
    [self setupTimer];
    [self.myTicker invalidate];
    stopCount = 0; // change start/stop back to start
}

// when you hit the back button it stops the timer
-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        [self.myTicker invalidate];
    }
      [super viewWillDisappear:animated];
}

- (void)fireTimer{
    self.myTicker = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showActivity) userInfo:nil repeats:YES];
}

// The Timer Part
- (void)setupTimer {
    secondsCount = 0;
    count = 0;
    action = steps[count];
    secondsPassed = 0;
    secondsLeft = [timeCount[secondsCount] intValue];
    current.text = [NSString stringWithFormat:@"Current: %@", steps[count]];
    count++;
    nextAction.text = [NSString stringWithFormat:@"Next: %@", steps[count]];
    count--;
    time.text = [NSString stringWithFormat:@"%i:0%i", secondsPassed, secondsPassed];
    downTime.text = [NSString stringWithFormat:@"Time for %@: %i", steps[count], secondsLeft];
        // Makes the Clock tick
    self.myTicker = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showActivity) userInfo:nil repeats:YES];
}

    // Determines when to show next action
- (void)showActivity{
    
  

    if (secondsLeft > 0 && (![action  isEqual: @"Prepare"]) ) {
        secondsPassed++;
    }
    if (secondsLeft > 0){
        secondsLeft--;
        downTime.text = [NSString stringWithFormat:@"Time for %@: %i", steps[count], secondsLeft];
    }else if (secondsLeft == 0 && ![steps[count+1] isEqual:@"Enjoy!"]) {
        count++;
        action = steps[count];
        secondsCount++;
        [buttonBeep play];
        current.text = [NSString stringWithFormat:@"Current: %@", steps[count]];
        count++;
        nextAction.text = [NSString stringWithFormat:@"Next: %@", steps[count]];
        count--;
        secondsLeft = [timeCount[secondsCount] intValue];
        downTime.text = [NSString stringWithFormat:@"Time for %@: %i", steps[count], secondsLeft];
    }
    else if ([steps[count+1] isEqual:@"Enjoy!"]){
        [buttonBeep play]; //play ending sound later
        current.text = [NSString stringWithFormat:@"Current: %@", steps[count + 1]];
        nextAction.text = [NSString stringWithFormat:@""];
        secondsLeft = 0;
        downTime.text = [NSString stringWithFormat:@"Enjoy your Joe!"];
        secondsPassed++;
    }
    if (![action  isEqual: @"Prepare"]){
    int secondsPart = secondsPassed % 60;
    int minutesPart = (int) floor(secondsPassed / 60.0);
    time.text = [NSString stringWithFormat:(secondsPart<10 ? @"%i:0%i" : @"%i:%i"), minutesPart, secondsPart];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    TimerViewController *tvc = [segue destinationViewController];
    // Get the new view controller using [segue destinationViewController].
    PhotoIcon *c = self.currentBrew;
    [tvc setCurrentBrew:c];
    
}


@end
